angular
    .module('PokemonApp')
    .factory('PokemonsService', function($resource, $http) {
        const dataUrl = 'https://api.backendless.com/D0A791EA-5141-7985-FFC8-CFCBEF5D6300/1C8B9B56-4AD9-5BF2-FFA9-2CEB6CF73000/data/Pokemons';

        return $resource( dataUrl + '/:pokemonId/', {
            pokemonId: '@pokemonId'
        }, {
            query: {
                method: 'GET',
                isArray: true,
            },
            update: {
                method: 'PUT'
            }
        })
    });
